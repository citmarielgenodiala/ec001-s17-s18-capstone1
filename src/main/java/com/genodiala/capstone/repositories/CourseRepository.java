package com.genodiala.capstone.repositories;

import com.genodiala.capstone.models.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course, Object> {
    Course findByCourseName(String courseName);
}
