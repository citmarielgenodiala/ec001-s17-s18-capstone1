package com.genodiala.capstone.models;

import javax.persistence.*;

@Entity
@Table(name="courses")
public class Course{

    @Id
    @GeneratedValue
    private int courseId;

    @Column
    private String courseName;

    @Column
    private String courseDescription;

    @Column
    private double price;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

// Stretch Goals
//    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
//    @JsonIgnore
//    private Set<CourseEnrollment> enrollees;

//    @Column
//    private boolean isActive;

    public Course(){};

    public Course(int courseId, String courseName, String courseDescription, double price, User user) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.price = price;
        this.user = user;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getCourseId() {
        return courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public double getPrice() {
        return price;
    }

    public User getUser() {
        return user;
    }

    // Stretch Goals : For future reference only.
//    public Set<CourseEnrollment> getEnrollees() {
//        return enrollees;
//    }
//
//    public boolean isActive() {
//        return isActive;
//    }
//
//    public void setActive(boolean isActive) {
//        this.isActive = isActive;
//    }

}

