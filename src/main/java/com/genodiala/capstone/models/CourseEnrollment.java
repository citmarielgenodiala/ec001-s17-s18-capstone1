package com.genodiala.capstone.models;

import javax.persistence.*;
import java.time.LocalDateTime;

// Stretch Goals: I only implemented the model. This is just a reference.
@Entity
@Table(name="course_enrollments")

public class CourseEnrollment {

    @Id
    @GeneratedValue
    private int courseEnrollmentId;


    @ManyToOne
    @JoinColumn(name = "course_id", referencedColumnName = "courseId")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "userId")
    private User user;
    @Column
    private LocalDateTime dateTimeEnrolled;

    public CourseEnrollment() {
    }

    public CourseEnrollment(int courseEnrollmentId, Course course, User user, LocalDateTime dateTimeEnrolled) {
        this.courseEnrollmentId = courseEnrollmentId;
        this.course = course;
        this.user = user;
        this.dateTimeEnrolled = dateTimeEnrolled;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setDateTimeEnrolled(LocalDateTime dateTimeEnrolled) {
        this.dateTimeEnrolled = dateTimeEnrolled;
    }

    public int getCourseEnrollmentId() {
        return courseEnrollmentId;
    }

    public Course getCourse() {
        return course;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getDateTimeEnrolled() {
        return dateTimeEnrolled;
    }
}