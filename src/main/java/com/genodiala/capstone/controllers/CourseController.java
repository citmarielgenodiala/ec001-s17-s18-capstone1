package com.genodiala.capstone.controllers;

import com.genodiala.capstone.models.Course;
import com.genodiala.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/courses")
public class CourseController {
    @Autowired
    CourseService courseService;

    //    Create Course
    @PostMapping("/add-course")
    public ResponseEntity<Object> addCourse(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        courseService.addCourse(stringToken, course);
        return new ResponseEntity<>("Course added successfully.", HttpStatus.CREATED);
    }

    //    Get all course
    @GetMapping("/get-all-courses")
    public ResponseEntity<Object> getCourses(){
        return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
    }

    //    Delete a course
    @DeleteMapping("/delete-course/{courseId}")
    public ResponseEntity<Object> deleteCourse(@PathVariable int courseId, @RequestHeader(value = "Authorization") String stringToken){
        return courseService.deleteCourse(courseId, stringToken);
    }

    //    Update a course
    @PutMapping("/update-course/{courseId}")
    public ResponseEntity<Object> updateCourse(@PathVariable int courseId, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        return courseService.updateCourse(courseId, stringToken, course);
    }

    //    Get user's courses
    @GetMapping("/my-courses")
    public ResponseEntity<Object> getMyCourses(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(courseService.getMyCourses(stringToken), HttpStatus.OK);
    }

}
