package com.genodiala.capstone.controllers;

import com.genodiala.capstone.exceptions.UserException;
import com.genodiala.capstone.models.User;
import com.genodiala.capstone.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    // Create user
   @PostMapping("/create-user")
    public ResponseEntity<Object> createUser(@RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity<>("User created successfully", HttpStatus.CREATED);
    }

    // Get users
    @GetMapping("/get-users")
    public ResponseEntity<Object> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    // Delete user
    @DeleteMapping("/delete-user/{userId}")
    public ResponseEntity<Object> deleteUser(@PathVariable int userId) {
        return userService.deleteUser(userId);
    }

    // Update user
    @PutMapping("/update-user/{userId}")
    public ResponseEntity<Object> updateUser(@PathVariable int userId, @RequestBody User user) {
        return userService.updateUser(userId, user);
    }

    // User registration
    @PostMapping("/register")
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException {
        String username = body.get("username");

        if(!userService.findByUsername(username).isEmpty()) {
            throw new UserException("Username already exists.");
        } else {
            String password = body.get("password");
            String encodedPassword = new BCryptPasswordEncoder().encode(password);
            User newUser = new User(username, encodedPassword);
            userService.createUser(newUser);
            return new ResponseEntity<>("User registered successfully", HttpStatus.CREATED);
        }

    }

}
