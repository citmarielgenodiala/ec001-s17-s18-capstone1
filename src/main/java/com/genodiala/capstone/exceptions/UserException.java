package com.genodiala.capstone.exceptions;

public class UserException extends Exception{

    public UserException(String message){ super(message); }

}
