package com.genodiala.capstone.services;

import com.genodiala.capstone.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    void addCourse(String stringToken, Course course);
    Iterable<Course> getCourses();
    ResponseEntity deleteCourse(int courseId, String stringToken);
    ResponseEntity updateCourse(int courseId, String stringToken, Course course);
    Iterable<Course> getMyCourses(String stringToken);


}
